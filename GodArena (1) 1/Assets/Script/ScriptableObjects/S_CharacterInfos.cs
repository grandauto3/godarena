﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CharacterInfos", menuName = "CharacterContainer")]
public class S_CharacterInfos : ScriptableObject
{
    [ReadOnly]
    public int currAmmo;
    
    public int totalAmmo;
    
    public float fireRate;


}