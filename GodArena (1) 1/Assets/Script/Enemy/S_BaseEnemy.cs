﻿//TODO: Chase too slow


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public abstract class S_BaseEnemy : MonoBehaviour
{

    #region MEMBERS

    #region ENUMS

    public enum State
    {
        Chase,
        Idle,
        Patrol,
        Attack,
    }

    #endregion

    #region INSPECTOR

    [Header("Properties")]
    [SerializeField]
    protected float health;
    [SerializeField]
    float damage;

    [Header("Pathfinding")]
    [SerializeField]
    protected Transform eyes;
    [SerializeField]
    protected float lookingDistance;
    [SerializeField]
    protected float lookingRadius;
    [SerializeField]
    GameObject player;
    [SerializeField]
    GameObject[] patrolPoints;

    #endregion

    #region NONINSPECTOR
    protected State state;

    protected S_BasePlayer playerScript;

    S_ExecutePlayer exePlayer;

    int destPoint;

    NavMeshAgent agent;

    #region ANIMATION

    Animator anim;
    AnimatorStateInfo stateInfo;

    Vector2 smoothDeltaPosition;
    Vector2 velocity;

    int forwardID;
    int turnID;
    int moveID;
    int attackID;
    int dyingID;

    #endregion

    #endregion

    #endregion

    protected virtual void OnValidate()
    {
        if (!GetComponentInChildren<NavMeshAgent>())
            agent = gameObject.AddComponent<NavMeshAgent>();

        if(!GetComponentInChildren<Animator>())
            anim = gameObject.AddComponent<Animator>();
    }

    void Awake()
    {
        OnValidate();

        #region PATROLPOINTS

        patrolPoints = GameObject.FindGameObjectsWithTag("Waypoint");

        #endregion

        #region PLAYERSCRIPT

        player = GameObject.FindGameObjectWithTag("Player");
        exePlayer = player.GetComponent<S_ExecutePlayer>();

        #endregion

        #region NAVMESH

        agent = GetComponent<NavMeshAgent>();

        #endregion

        #region ANIMATION

        anim = GetComponentInChildren<Animator>();
        
        #endregion
    }

    // Use this for initialization
    protected virtual void Start()
    {
        S_GameManager.enemyList.Add(gameObject);

        #region NAVMESH

        agent.updatePosition = false;

        #endregion

        #region ANIMATION

        stateInfo = anim.GetCurrentAnimatorStateInfo(0);

        smoothDeltaPosition = Vector2.zero;
        velocity = Vector2.zero;

        forwardID = Animator.StringToHash("Forward");
        turnID = Animator.StringToHash("Turn");
        moveID = Animator.StringToHash("move");
        attackID = Animator.StringToHash("attackT");
        dyingID = Animator.StringToHash("Died");

        //anim.SetBool(moveID, false);

        #endregion

        #region FSM

        state = State.Chase;

        #endregion

    }

    // Update is called once per frame
    protected virtual void Update()
    {
        #region DEATH

        if (health <= 0f)
        {
            anim.SetTrigger(dyingID);
            S_GameManager.RemoveGameObjectFromEnemyList(gameObject);
            Destroy(gameObject, 5f);
            return;
        }

        #endregion

        playerScript = S_ExecutePlayer.GetCurrentScript(exePlayer.currCharacter);

        InputManager();
    }

    void OnTriggerEnter(Collider other)
    {
        #region TRIGGERPLAYER

        if (other.tag == "Player")
        {
            ChangeStateToAttack();
        }

        #endregion

        #region TRIGGERBULLET

        if (other.tag == "Bullet")
        {
            GetDamage(playerScript.damage);
            Destroy(other.gameObject);
        }

        #endregion
    }

    void OnTriggerExit(Collider other)
    {
        #region TRIGGERPLAYER

        if (playerScript.isDead)
            return;
        if (other.tag == "Player")
        {
            ChangeStateToChase();
        }

        #endregion
    }

    void InputManager()
    {
        DoState();
        //LookingForPlayer();
        CheckPlayerIsNear();
        AnimationMovement();
    }

    #region FSM

    void DoState()
    {
        switch (state)
        {
            case State.Chase:

                Move(player.transform.position);

                break;
            case State.Idle:

                Move(Vector3.zero);

                break;
            case State.Patrol:

                GoToNextPoint();

                break;
            case State.Attack:

                if(!playerScript.isDead)
                    Attack();
                else
                    ChangeStateToPatrol();

                break;


            default:
                break;
        }
    }

    void Move(Vector3 pos)
    {
        if (pos == Vector3.zero)
            return;

        agent.destination = pos;
    }
    void GoToNextPoint()
    {
        if (patrolPoints.Length == 0)
        {
            Debug.LogWarning("No Patrol Points set");

            return;
        }

        if (!agent.pathPending && agent.remainingDistance <= 0.5f)
        {
            agent.destination = patrolPoints[destPoint].transform.position;

            destPoint = Random.Range(0, patrolPoints.Length);
        }
    }

    void Attack()
    {
        if(playerScript.isDead)
        {
            ChangeStateToPatrol();
            return;
        }

        anim.SetTrigger(attackID);
        StartCoroutine(Attack(damage));
    }

    protected virtual IEnumerator Attack(float dmg)
    {

        yield return new WaitForSeconds(stateInfo.length / 6f);

        playerScript.health -= (dmg / ((float)playerScript.ArmorUpgrade / 3f)) * Time.deltaTime;

        yield return new WaitUntil(() => stateInfo.normalizedTime < 1.0f);
    }
    protected virtual void LookingForPlayer()
    {
        if (playerScript.isDead)
            return;

        RaycastHit hit;
        if (Physics.SphereCast(eyes.position, lookingRadius, eyes.forward, out hit, lookingDistance))
        {
            if (hit.collider.tag == "Player")
            {
                ChangeStateToChase();
            }
        }
    }
    void CheckPlayerIsNear()
    {
        if(Vector3.Distance(transform.position, player.transform.position) < lookingDistance)
            ChangeStateToChase();
    }

    protected void ChangeStateToChase()
    {
        state = State.Chase;
    }
    protected void ChangeStateToIdle()
    {
        state = State.Idle;
    }
    protected void ChangeStateToPatrol()
    {
        state = State.Patrol;
    }
    protected void ChangeStateToAttack()
    {
        state = State.Attack;
    }

    #endregion

    public virtual void GetDamage(float dmg)
    {
        health -= dmg;

        ChangeStateToChase();
    }

    #region ANIMATION
    
    void AnimationMovement()
    {
        Vector3 worldDeltaPos = agent.nextPosition - transform.position;


        float dx = Vector3.Dot(transform.right, worldDeltaPos);
        float dy = Vector3.Dot(transform.forward, worldDeltaPos);

        Vector2 deltaPos = new Vector2(dx, dy);

        float smooth = Mathf.Min(1f, Time.deltaTime / .15f);

        smoothDeltaPosition = Vector2.Lerp(smoothDeltaPosition, deltaPos, smooth);

        if(Time.deltaTime > 1e-5f)
            velocity = smoothDeltaPosition / Time.deltaTime;

        bool shouldMove = velocity.magnitude > .5f && agent.remainingDistance > agent.radius;

        anim.SetBool(moveID, shouldMove);
        anim.SetFloat(turnID, velocity.x);
        anim.SetFloat(forwardID, velocity.y);

        // Pull agent towards character
        if(worldDeltaPos.magnitude > agent.radius)
            agent.nextPosition = transform.position + 0.9f * worldDeltaPos;
    }

    void OnAnimatorMove()
    {
        Vector3 position = anim.rootPosition;
        position.y = agent.nextPosition.y;
        transform.position = position;
    }

    #endregion
}
