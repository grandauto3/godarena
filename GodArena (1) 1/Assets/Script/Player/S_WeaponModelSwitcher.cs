﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S_WeaponModelSwitcher : MonoBehaviour
{

	[SerializeField]
	GameObject[] weaponModel;

	S_ExecutePlayer exe;

	void Awake()
	{
		exe = GameObject.Find("Player").GetComponent<S_ExecutePlayer>();

		for(int i = 0; i < weaponModel.Length; i++)
		{
			weaponModel[i].SetActive(false);
		}
	}

	// Update is called once per frame
	void Update()
	{
		switch(exe.currCharacter)
		{
			case Character.Rolf:
				
				for(int i = 0; i < weaponModel.Length; i++)
				{
					if(i == 0)
					{
						weaponModel[i].SetActive(true);
						continue;
					}
					weaponModel[i].SetActive(false);
				}

				break;
			case Character.Lisa:

				for(int i = 0; i < weaponModel.Length; i++)
				{
					if(i == 1)
					{
						weaponModel[i].SetActive(true);
						continue;
					}
					weaponModel[i].SetActive(false);
				}

				break;
			case Character.Thorsten:

				for(int i = 0; i < weaponModel.Length; i++)
				{
					if(i == 2)
					{
						weaponModel[i].SetActive(true);
						continue;
					}
					weaponModel[i].SetActive(false);
				}

				break;
			case Character.Jane:

				for(int i = 0; i < weaponModel.Length; i++)
				{
					if(i == 3)
					{
						weaponModel[i].SetActive(true);
						continue;
					}
					weaponModel[i].SetActive(false);
				}

				break;
			default:
				break;
		}
	}
}
