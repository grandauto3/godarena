﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S_LisaController : S_BasePlayer
{
    #region PROPERTIES
    
    public override int DamageUpgrade
    {
        get { return damageUpgrade; }
        set { damageUpgrade = value; }
    }

    public override int HealthUpgrade
    {
        get { return healthUpgrade; }
        set { healthUpgrade = value; }
    }
    public override int ArmorUpgrade
    {
        get { return armorUpgrade; }
        set { armorUpgrade = value; }
    }
    public override int SpecialAbilityUpgrade
    {
        get { return specialUpgrade; }
        set { specialUpgrade = value; }
    }

    #endregion

    [Header("Heal")]
    [SerializeField]
    float healPower;

    S_BasePlayer[] chars;

    // Use this for initialization
    protected override void Start()
    {
        base.Start();

        chars = new S_BasePlayer[] 
        {
            rolfScript,
            lisaScript,
            thorstenScript,
            janeScript
        };
    }

    void Heal(float healAmount)
    {
        for (int i = 0; i < chars.Length; i++)
        {
            if(chars[i].health >= chars[i].healthTotal)
            {
                chars[i].health = chars[i].healthTotal;
                continue;
            }
            chars[i].health += (chars[i].healthTotal * healAmount) / 100;
        }
    }

    protected override void SpecialAbility()
    {
        if(Input.GetKeyDown(KeyCode.F))
        {
            Heal(healPower * (specialUpgrade * 1.5f));
        }
    }

    protected override void SpecialAbilityFixed()
    {

    }

    protected override void GetInfo()
    {
        weaponType = WeaponType.AutoPistol;
        character = Character.Lisa;
    }
}
