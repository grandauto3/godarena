﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S_JaneController : S_BasePlayer
{

	#region PROPERTIES

	public override int DamageUpgrade
	{
		get { return damageUpgrade; }
		set { damageUpgrade = value; }
	}

	public override int HealthUpgrade
	{
		get { return healthUpgrade; }
		set { healthUpgrade = value; }
	}
	public override int ArmorUpgrade
	{
		get { return armorUpgrade; }
		set { armorUpgrade = value; }
	}
	public override int SpecialAbilityUpgrade
	{
		get { return specialUpgrade; }
		set { specialUpgrade = value; }
	}

	#endregion

	protected override void SpecialAbility()
	{

	}

	protected override void SpecialAbilityFixed()
	{

	}

	//// Use this for initialization
	protected override void Start()
	{
		base.Start();
	}

	protected override void GetInfo()
	{
		weaponType = WeaponType.SingleRifle;
		character = Character.Jane;
	}
}
