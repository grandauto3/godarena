﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S_RolfController : S_BasePlayer
{
	[Header("Grenade")]
	[SerializeField]
	float grenadeSpeed;

	[SerializeField]
	GameObject grenadeSpawn;
	[SerializeField]
	GameObject grenade;
	[SerializeField]
	int grenadeCount;

	[HideInInspector]
	public int currentCount;

	bool isGInput = false;
	
	public override int DamageUpgrade
	{
		get { return damageUpgrade; }
		set { damageUpgrade = value; }
	}

	public override int HealthUpgrade
	{
		get { return healthUpgrade; }
		set { healthUpgrade = value; }
	}
	public override int ArmorUpgrade
	{
		get { return armorUpgrade; }
		set { armorUpgrade = value; }
	}
	public override int SpecialAbilityUpgrade
	{
		get { return specialUpgrade; }
		set { specialUpgrade = value; }
	}

	// Use this for initialization
	protected override void Start()
	{
		base.Start();
		isGInput = false;

		if (grenade.GetComponent<Rigidbody>())
			grenade.GetComponent<Rigidbody>();
		else
			grenade.AddComponent<Rigidbody>();


	}

	void ThrowGranade()
	{
		if(currentCount >= grenadeCount)
			return;

		GameObject tmp = Instantiate(grenade, grenadeSpawn.transform.position, grenadeSpawn.transform.rotation);
		tmp.GetComponent<Rigidbody>().AddForce(grenadeSpawn.transform.forward * grenadeSpeed, ForceMode.Impulse);
		tmp.GetComponent<S_GrenadeController>().rolfScript = this;
		tmp.GetComponent<S_GrenadeController>().damage *= 1.5f;
		currentCount++;
	}

	protected override void SpecialAbility()
	{
		if (Input.GetKeyDown(KeyCode.G))
			isGInput = true;
	}

	protected override void SpecialAbilityFixed()
	{
		if (isGInput)
		{
			ThrowGranade();
			isGInput = false;
		}
	}

	protected override void GetInfo()
	{
		weaponType = WeaponType.Rifle;
		character = Character.Rolf;
	}
}
