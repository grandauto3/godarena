﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S_ThorstenController : S_BasePlayer
{
    #region MEMBERS

    #region PROPERTIES

    public override int DamageUpgrade
    {
        get { return damageUpgrade; }
        set { damageUpgrade = value; }
    }

    public override int HealthUpgrade
    {
        get { return healthUpgrade; }
        set { healthUpgrade = value; }
    }
    public override int ArmorUpgrade
    {
        get { return armorUpgrade; }
        set { armorUpgrade = value; }
    }
    public override int SpecialAbilityUpgrade
    {
        get { return specialUpgrade; }
        set { specialUpgrade = value; }
    }

    #endregion

    #region INSPECTOR

    [Header("Turret")]
    [SerializeField]
    GameObject turret;
    [SerializeField]
    float cooldown;
    [SerializeField]
    float turretLifeSpan;
    [SerializeField]
    int totalTurrets;


    #endregion

    float lastTurret;
    [HideInInspector]
    public List<GameObject> turretList;

    #endregion

    protected override void GetInfo()
    {
        weaponType = WeaponType.MG;
        character = Character.Thorsten;
    }

    //// Use this for initialization
    protected override void Start()
    {
        base.Start();

        turretList = new List<GameObject>();

        turretList.TrimExcess();

        lastTurret = -cooldown;
    }

    protected override void SpecialAbility()
    {
        if (Input.GetKeyDown(KeyCode.F))
            SetTurret();
    }

    protected override void SpecialAbilityFixed()
    {
        
    }

    void SetTurret()
    {
        turretList.TrimExcess();
        Vector3 pos = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);

        float t = Time.time;

        if((t - lastTurret) >= cooldown && turretList.Count <= totalTurrets * specialUpgrade)
        {
            //GameObject tmptur = Instantiate(turret, pos, Quaternion.identity);

            //turretList.Add(tmptur);
            //tmptur.GetComponent<S_TurretScript>().thorstenScrrr = this;

            //Destroy(tmptur, turretLifeSpan);

            //lastTurret = t;

            if(S_GameManager.turrentQueue.Count > 0)
            {
                GameObject tmp = S_GameManager.turrentQueue.Dequeue();

                tmp.transform.position = pos;
                tmp.transform.rotation = Quaternion.identity;
                turretList.Add(tmp);

                tmp.SetActive(true);
            }
        }
    }
}
