﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S_TurretScript : MonoBehaviour
{
	[Header("General")]
	[SerializeField]
	Transform turretHead;
	[SerializeField]
	GameObject impactEffect;
	[SerializeField]
	float lifeTime;

	[Header("Weapon")]
	[SerializeField]
	GameObject bulletSpawn;
	[SerializeField]
	float damage;
	[SerializeField]
	float attackRange;
	[SerializeField]
	float distance;
	[SerializeField]
	float fireRate;

	GameObject[] enemy;
	Transform nearestEnemy;
	AudioSource audio;

	float dis;
	float lastBullet;
	float startTime;

	void Awake()
	{
		audio = GetComponent<AudioSource>();
	}

	void OnEnable()
	{
		enemy = S_GameManager.enemyList.ToArray();

		lastBullet = -fireRate;
		startTime = lifeTime + Time.time;
	}

	// Update is called once per frame
	void Update ()
	{
		DisableAfterTime();

		Rotate();


		if (!nearestEnemy)
			return;
		Shoot();
	}

	void DisableAfterTime()
	{
		if(startTime <= Time.time)
		{
			S_GameManager.turrentQueue.Enqueue(gameObject);
			gameObject.SetActive(false);
		}
	}


	void Rotate()
	{
		nearestEnemy = GetNearestTransform();
		if(nearestEnemy == null)
		{
			return;
		}

		Vector3 direction = nearestEnemy.position - transform.position;
		direction.y = 0f;

		Quaternion rot = Quaternion.LookRotation(direction);

		turretHead.rotation = rot;
	}

	Transform GetNearestTransform()
	{
		enemy = S_GameManager.enemyList.ToArray();

		Transform target = null;
		float nearestDis = .0f;

		for (int i = 0; i < enemy.Length; i++)
		{
			if (enemy[i] == null) break;

			dis = Vector3.Distance(enemy[i].transform.position, transform.position);
			
			if(dis <= distance)
			{
				if (i == 0)
				{
					nearestDis = dis;
					target = enemy[i].transform;
				}
				else
				{
					if (nearestDis > dis)
					{
						nearestDis = dis;
						target = enemy[i].transform;
					}
				}
			}
		}


		return target;
	}

	void Shoot()
	{
		float t = Time.time;
		if((t - lastBullet) >= fireRate)
		{
			RaycastHit hit;

			if(Physics.Raycast(bulletSpawn.transform.position, bulletSpawn.transform.forward, out hit, attackRange))
			{
				if (hit.transform.tag == "Enemy")
					nearestEnemy.GetComponent<S_NormalEnemyController>().GetDamage(damage);

				GameObject tmp = Instantiate(impactEffect, hit.point, Quaternion.LookRotation(hit.normal));

				audio.Play();


				Destroy(tmp, 1f);
			}

			lastBullet = t;
		}
	}
}
