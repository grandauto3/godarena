﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class S_ExecutePlayer : MonoBehaviour
{
    [SerializeField]
    public Character currCharacter;

    S_BasePlayer currScript;

    S_RolfController rolfScript;
    S_LisaController lisaScript;
    S_ThorstenController thorstenScript;
    S_JaneController janeScript;

    static S_RolfController s_rolfScript;
    static S_LisaController s_lisaScript;
    static S_ThorstenController s_thorstenScript;
    static S_JaneController s_janeScript;

    void OnValidate()
    {
        if (!rolfScript && !s_rolfScript)
        {
            rolfScript = GetComponent<S_RolfController>();
            s_rolfScript = GetComponent<S_RolfController>();
        }
        if (!lisaScript && !s_lisaScript)
        {
            lisaScript = GetComponent<S_LisaController>();
            s_lisaScript = GetComponent<S_LisaController>();
        }
        if (!thorstenScript && !s_thorstenScript)
        {
            thorstenScript = GetComponent<S_ThorstenController>();
            s_thorstenScript = GetComponent<S_ThorstenController>();
        }
        if (!janeScript && !s_janeScript)
        {
            janeScript = GetComponent<S_JaneController>();
            s_janeScript = GetComponent<S_JaneController>();
        }
    }

    void Awake()
    {
        OnValidate();
    }

    void Update()
    {
        currScript = GetCurrentScript(currCharacter);

        currScript.InputManager();
    }

    void FixedUpdate()
    {
        currScript = GetCurrentScript(currCharacter);

        currScript.FixedInputManager();
    }



    public static S_BasePlayer GetCurrentScript(Character character)
    {
        S_BasePlayer currScript;

        switch (character)
        {
            case Character.Rolf:

                currScript = s_rolfScript;

                break;
            case Character.Lisa:

                currScript = s_lisaScript;

                break;
            case Character.Thorsten:

                currScript = s_thorstenScript;

                break;
            case Character.Jane:

                currScript = s_janeScript;

                break;
            default:

                currScript = null;

                Debug.LogWarning("No Script set");
                break;
        }

        return currScript;
    }
}
