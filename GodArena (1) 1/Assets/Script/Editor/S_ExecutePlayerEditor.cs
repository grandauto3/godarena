﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(S_ExecutePlayer))]
[CanEditMultipleObjects]
public class S_ExecutePlayerEditor : Editor
{
    S_ExecutePlayer script;

    S_RolfController rolfScript;
    S_LisaController lisaScript;
    S_ThorstenController thorstenScript;
    S_JaneController janeScript;

    S_BasePlayer currScript;

    SerializedProperty infos;
    SerializedProperty currChar;

    SerializedObject currObj;

    Editor scriptE; 


    void OnEnable()
    {
        script = target as S_ExecutePlayer;
        GetAllScripts();
    }

    public override void OnInspectorGUI()
    {
        currChar = serializedObject.FindProperty("currCharacter");
        Character charObj = (Character)currChar.enumValueIndex;

        DrawDefaultInspector();

        currScript = GetCurrentScript(script.currCharacter);

        Editor e = CreateEditor(currScript);

        currObj = new SerializedObject(currScript);
        infos = currObj.FindProperty("weaponInfos");

        CreateCachedEditor(infos.objectReferenceValue, null, ref scriptE);

        e.DrawDefaultInspector();

        if(scriptE)
        {
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Weapons", EditorStyles.boldLabel);

            scriptE.DrawDefaultInspector();
        }
        
        serializedObject.ApplyModifiedProperties();
    }

    void DisplayReadOnlyValue(string label, int value)
    {
        using (new EditorGUI.DisabledScope(true))
        {
            EditorGUILayout.IntField(label, value);
        }
    }
    void DisplayReadOnlyValue(string label, float value)
    {
        using (new EditorGUI.DisabledScope(true))
        {
            EditorGUILayout.FloatField(label, value);
        }
    }
    void DisplayReadOnlyValue(string label, string value)
    {
        using (new EditorGUI.DisabledScope(true))
        {
            EditorGUILayout.TextField(label, value);
        }
    }

    void GetAllScripts()
    {
        rolfScript = script.GetComponent<S_RolfController>();
        lisaScript = script.GetComponent<S_LisaController>();
        thorstenScript = script.GetComponent<S_ThorstenController>();
        janeScript = script.GetComponent<S_JaneController>();
    }

    S_BasePlayer GetCurrentScript(Character character)
    {
        S_BasePlayer currScript;

        switch (character)
        {
            case Character.Rolf:
                currScript = rolfScript;
                break;
            case Character.Lisa:
                currScript = lisaScript;
                break;
            case Character.Thorsten:
                currScript = thorstenScript;
                break;
            case Character.Jane:
                currScript = janeScript;
                break;
            default:
                currScript = null;
                Debug.LogWarning("Editor: CurrentScript couldn't be set!");
                break;
        }

        return currScript;
    }
}