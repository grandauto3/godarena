﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class S_GameManager : MonoBehaviour
{
    #region MEMBERS

    #region INSPECTOR

    [SerializeField]
    GameObject enemy;
    [SerializeField]
    Transform[] spawnPoints;
    [SerializeField]
    float spawnRate;
    [SerializeField]
    int enemyCount;
    [SerializeField]
    GameObject player;
    [SerializeField]
    GameObject turrent;
    [SerializeField]
    int turrentCount;
    
    GameObject[] enemys;

    #endregion

    #region PUBLIC
    [HideInInspector]
    public static List<GameObject> enemyList;
    [HideInInspector]
    public static List<Character> deathList;
    [HideInInspector]
    public static int round;
    [HideInInspector]
    public static Queue<GameObject> turrentQueue;
    [HideInInspector]
    static bool isEnemiesDead;

    #endregion

    #endregion

    void Awake()
    {
        enemyList = new List<GameObject>();
        deathList = new List<Character>();
        turrentQueue = new Queue<GameObject>();
    }

    // Use this for initialization
    void Start()
    {
        isEnemiesDead = false;
        enemyCount *= round;
        enemyCount /= 2;
        enemyList.Capacity = enemyCount;
        StartCoroutine(SpawnEnemies());

        for(int i = 0; i < turrentCount; i++)
        {
            GameObject tmp = Instantiate(turrent, Vector3.zero, Quaternion.identity);
            turrentQueue.Enqueue(tmp);
            tmp.SetActive(false);
        }

    }

    IEnumerator SpawnEnemies()
    {
        int spawnIndex = 0;

        while (enemyCount > 0)
        {
            Instantiate(enemy, spawnPoints[spawnIndex].position, spawnPoints[spawnIndex].rotation);

            spawnIndex = (spawnIndex + 1) % spawnPoints.Length;

            enemyCount--;

            yield return new WaitForSeconds(spawnRate);
        }
    }

    public static void UpdateEnemyList()
    {
        enemyList.TrimExcess();
    }
    public static void RemoveGameObjectFromEnemyList(GameObject obj)
    {
        if (enemyList.Contains(obj))
            enemyList.Remove(obj);
        enemyList.TrimExcess();
    }

    public static bool EnemiesDead()
    {
        if(enemyList.Capacity <= 0)
            isEnemiesDead = true;

        return isEnemiesDead;
    }

    public static void RoundCounter()
    {
        ++round;
    }
    
    public static void ResetRoundCounter()
    {
        round = 0;
    }

    public static float CalculatePlayerHealth()
    {
        float totalHealth = 0;

        foreach (Character item in Enum.GetValues(typeof(Character)))
        {
            S_BasePlayer scrrScript = S_ExecutePlayer.GetCurrentScript(item);

            totalHealth += scrrScript.health;
        }

        return totalHealth;
    }

    public static bool IsPlayerDead()
    {
        deathList.TrimExcess();
        if (deathList.Count >= Enum.GetNames(typeof(Character)).Length)
            return true;

        return false;
    }
}
