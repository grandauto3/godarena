﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S_BulletController : MonoBehaviour
{
	[SerializeField]
	float speed;

	// Use this for initialization
	void Start ()
	{
		//speed = 30;
	}
	
	// Update is called once per frame
	void Update ()
	{
		Movement();

		Destroy(gameObject, 2f);
	}

	void Movement()
	{
		transform.position += transform.forward * speed * Time.deltaTime;
	}
}
