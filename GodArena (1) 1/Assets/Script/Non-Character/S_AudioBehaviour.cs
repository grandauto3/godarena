﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class S_AudioBehaviour : MonoBehaviour
{
    static S_AudioBehaviour instance;

    public static S_AudioBehaviour Instance
    {
        get { return instance; }
    }

    void Awake()
    {
        if(instance != null && instance != this)
        {
            Destroy(gameObject);
            return;
        }
        else
        {
            instance = this;
        }

        DontDestroyOnLoad(gameObject);
    }

    void Update()
    {
        if(SceneManager.GetActiveScene().buildIndex <= 0)
        {
            Destroy(gameObject);
            Destroy(instance.gameObject);
            return;
        }
    }
}
