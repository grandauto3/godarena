﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public enum Upgrades
{
	Damage,
	Health,
	Armor,
	SpecialAbility,
}

public class S_UIManager : MonoBehaviour
{
	#region GENERAL

	S_BasePlayer player;
	[SerializeField]
	S_ExecutePlayer exePlayer;
	[SerializeField]
	S_BaseEnemy enemy;
	[SerializeField]
	S_GameManager gameManager;
	[HideInInspector]
	public bool isMouseLocked;

	float totalHealth;

	#endregion

	#region HUD
	[Header("HUD")]
	[SerializeField]
	Text roundsCounter;
	[SerializeField]
	Text enemysCounter;
	[Space]
	[SerializeField]
	Text health;
	[SerializeField]
	Text ammo;
	[SerializeField]
	Slider healthSlider;
	[Space]
	[SerializeField]
	Image rolf;
	[SerializeField]
	Image lisa;
	[SerializeField]
	Image thorsten;
	[SerializeField]
	Image jane;
	[Space]
	[SerializeField]
	MaskableGraphic[] winPanel;
	[SerializeField]
	float showWinDuration;
	[SerializeField]
	MaskableGraphic[] losePanel;

	float timer;
	bool alreadyShowedLose;

	#endregion

	#region PAUSEMENU

	[Header("Pause Menu")]
	[SerializeField]
	GameObject pauseMenu;
	[SerializeField]
	GameObject optionsMenu;
	[Space]
	[SerializeField]
	Button resume;
	[SerializeField]
	Button options;
	[SerializeField]
	Button exitToMainMenu;
	[SerializeField]
	Button exitGame;

	#region OPTIONS

	[Space]

	[SerializeField]
	Dropdown resolutionDropdown;
	[SerializeField]
	Dropdown fullscreenDropdown;
	[SerializeField]
	Dropdown qualityDropdown;
	[SerializeField]
	Slider volumeSlider;
	[SerializeField]
	Button backButton;

	[SerializeField]
	AudioMixer mainMixer;

	//private
	Resolution[] resolutions;

	#endregion

	#endregion

	#region UPGRADEMENU

	[Header("Upgrade Menu")]
	[SerializeField]
	GameObject upgradeMenu;
	[Space]
	[SerializeField]
	Text damageValueText;
	[SerializeField]
	Text healthValueText;
	[SerializeField]
	Text armorValueText;
	[SerializeField]
	Text specialabilityValueText;
	[Space]
	[SerializeField]
	Text availableSkillpts;
	[SerializeField]
	int spendablePoints;
	[Space]
	[SerializeField]
	MaskableGraphic[] restartPanel;

	//statics
	[HideInInspector]
	public static int damageValue;
	[HideInInspector]
	public static int healthValue;
	[HideInInspector]
	public static int armorValue;
	[HideInInspector]
	public static int specialabilityValue;

	#endregion

	// Use this for initialization
	void Start()
	{
		isMouseLocked = true;

		//set lose Panel Alpha = 0
		for (int i = 0; i < losePanel.Length; i++)
		{
			losePanel[i].GetComponent<CanvasRenderer>().SetAlpha(.0f);
			losePanel[i].gameObject.SetActive(false);
		}
		//set win Panel Alpha = 0
		for (int i = 0; i < winPanel.Length; i++)
		{
			winPanel[i].GetComponent<CanvasRenderer>().SetAlpha(.0f);
			winPanel[i].gameObject.SetActive(false);
		}
		//set restart Panel Alpha = 0
		for (int i = 0; i < restartPanel.Length; i++)
		{
			restartPanel[i].GetComponent<CanvasRenderer>().SetAlpha(.0f);
			restartPanel[i].gameObject.SetActive(false);
		}

		#region LOCKMOUSE

		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;

		#endregion

		#region HUD
		ShowEnemyCounter();
		alreadyShowedLose = false;
		S_GameManager.RoundCounter();
		#endregion

		#region PAUSEMENU

		pauseMenu.SetActive(false);

		#region OPTIONS

		optionsMenu.SetActive(false);
		SetResolutionsDropdown();
		SetQuality();

		#endregion

		#endregion

		#region UPGRADEMENU

		if(damageValue == default(int) | healthValue == default(int) | armorValue == default(int) | specialabilityValue == default(int))
		{
			damageValue = 1;
			healthValue = 1;
			armorValue = 1;
			specialabilityValue = 1;
		}

		damageValueText.text = damageValue.ToString();
		healthValueText.text = healthValue.ToString();
		armorValueText.text = armorValue.ToString();
		specialabilityValueText.text = specialabilityValue.ToString();

		upgradeMenu.SetActive(false);

		#endregion
	}

	// Update is called once per frame
	void LateUpdate()
	{
		InputManager();
	}

	void InputManager()
	{
		ShowEnemyCounter();
		ShowRoundCounter();

		player = S_ExecutePlayer.GetCurrentScript(exePlayer.currCharacter);

		ToggleMouseLock(Input.GetButtonDown("Cancel"));

		#region HUD
		PlayerHealth();
		Ammunition();
		SetCharacterArrow();

		if (S_GameManager.IsPlayerDead() && !alreadyShowedLose)
			ShowLose();

		
		if (S_GameManager.EnemiesDead())
		{

			ShowWin();
			timer += Time.deltaTime;
			if(showWinDuration <= timer)
			{
				HideWin();
				SceneManager.LoadScene(1);
			}

		}
		#endregion

		#region PAUSEMENU

		ToggleMenu(Input.GetButtonDown("Cancel"));

		#endregion

		#region UPGRADEMENU

		ToggleUpgradeMenu(Input.GetButtonDown("UpgradeMenu"));

		#endregion
	}

	void ToggleMouseLock(bool inputValue)
	{
		if (!inputValue)
			return;

		if (inputValue)
		{
			isMouseLocked = !isMouseLocked;
		}

		if(isMouseLocked)
		{
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;
		}
		else
		{
			Cursor.lockState = CursorLockMode.None;
			Cursor.visible = true;
		}
	}
	void SetMouseLock(bool inputValue)
	{
		if (inputValue)
		{
			Cursor.lockState = CursorLockMode.None;
			Cursor.visible = true;
			
		}
		else
		{
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;
		}
	}

	#region HUD

	void SetCharacterArrow()
	{
		switch (exePlayer.currCharacter)
		{
			case Character.Rolf:
				
				rolf.enabled = true;
				lisa.enabled = false;
				thorsten.enabled = false;
				jane.enabled = false;

				break;
			case Character.Lisa:
				
				rolf.enabled = false;
				lisa.enabled = true;
				thorsten.enabled = false;
				jane.enabled = false;

				break;
			case Character.Thorsten:
				
				rolf.enabled = false;
				lisa.enabled = false;
				thorsten.enabled = true;
				jane.enabled = false;

				break;
			case Character.Jane:
				
				rolf.enabled = false;
				lisa.enabled = false;
				thorsten.enabled = false;
				jane.enabled = true;

				break;
			default:

				health.text = "Error";
				ammo.text = "Error";

				break;
		}
	}

	void PlayerHealth()
	{
		float health;
		health = (int)player.health;

		if(health <= 0f)
		{
			healthSlider.value = 0;
			this.health.text = 0 + "/" + player.healthTotal;
			return;
		}

		this.health.text = health.ToString() + "/" + player.healthTotal;

		health /= 100;
		healthSlider.value = health;
	}
	void Ammunition()
	{
		ammo.text = player.weaponInfos.currAmmo + "/" + player.weaponInfos.totalAmmo;
	}

	void ShowLose()
	{
		SetMouseLock(true);
		for (int i = 0; i < losePanel.Length; i++)
		{
			losePanel[i].gameObject.SetActive(true);
			FadeIn(losePanel[i]);
		}
	}

	void ShowWin()
	{
		for (int i = 0; i < winPanel.Length; i++)
		{
			winPanel[i].gameObject.SetActive(true);
			FadeIn(winPanel[i]);
		}
	}

	void HideWin()
	{
		for (int i = 0; i < winPanel.Length; i++)
		{
			FadeOut(winPanel[i]);
			winPanel[i].gameObject.SetActive(false);
		}
	}

	void ShowEnemyCounter()
	{
		S_GameManager.UpdateEnemyList();
		enemysCounter.text = "Enemies: " + S_GameManager.enemyList.Count;
	}

	void ShowRoundCounter()
	{
		roundsCounter.text = "Round: " + S_GameManager.round;
	}

	public void LoseNextButton()
	{
		if(!upgradeMenu.activeSelf)
		{
			ToggleUpgradeMenu(true);

			for (int i = 0; i < losePanel.Length; i++)
			{
				losePanel[i].GetComponent<CanvasRenderer>().SetAlpha(.0f);
				losePanel[i].gameObject.SetActive(false);
			}
			alreadyShowedLose = true;
		}
	}

	#endregion

	#region PAUSEMENU

	void ToggleMenu(bool inputValue)
	{
		if(inputValue)
		{
			pauseMenu.SetActive(!pauseMenu.activeSelf);
		}
	}

	public void Resume()
	{
		ToggleMenu(true);
		ToggleMouseLock(true);
	}
	public void Options()
	{
		pauseMenu.SetActive(false);
		optionsMenu.SetActive(true);
	}
	public void ExitToMainMenu()
	{
		SceneManager.LoadScene(0);
	}
	public void ExitGame()
	{
		Application.Quit();
	}

	#region OPTIONS

	public void ChangeResolution(int resolutionIndex)
	{
		Resolution res = resolutions[resolutionIndex];
		Screen.SetResolution(res.width, res.height, Screen.fullScreen);
	}

	public void ChangeFullscreen(int fullscreenIndex)
	{
		switch (fullscreenIndex)
		{
			case 0:
				Screen.fullScreen = true;
				break;

			case 1:
				
				break;

			case 2:

				Screen.fullScreen = false;
				break;


			default:
				Screen.fullScreen = false;
				break;
		}
	}

	public void ChangeQuality(int qualityIndex)
	{
		QualitySettings.SetQualityLevel(qualityIndex);
	}

	public void ChangeMasterVolume(float value)
	{
		mainMixer.SetFloat("MainVolume", value);
	}

	public void BackToMenu()
	{
		optionsMenu.SetActive(false);
		pauseMenu.SetActive(true);
	}

	void SetQuality()
	{
		qualityDropdown.value = QualitySettings.GetQualityLevel();
		qualityDropdown.RefreshShownValue();
	}

	void SetResolutionsDropdown()
	{
		resolutions = Screen.resolutions;

		resolutionDropdown.ClearOptions();

		List<string> options = new List<string>();

		int currRes = 0;
		for (int i = 0; i < resolutions.Length; i++)
		{
			string option = resolutions[i].width + " x " + resolutions[i].height;
			options.Add(option);

			if (Screen.currentResolution.width == resolutions[i].width && Screen.height == resolutions[i].height)
			{
				currRes = i;
			}
		}

		resolutionDropdown.AddOptions(options);
		resolutionDropdown.value = currRes;
		resolutionDropdown.RefreshShownValue();
	}

	#endregion

	#endregion

	#region UPGRADEMENU

	void ToggleUpgradeMenu(bool inputValue)
	{
		if(inputValue)
		{
			ToggleMouseLock(true);
			upgradeMenu.SetActive(!upgradeMenu.activeSelf);
			availableSkillpts.text = spendablePoints.ToString();
		}
	}

	public void IncreaseDamage(Text val)
	{
		IncreaseValue(val, Upgrades.Damage);
	}
	public void IncreaseHealth(Text val)
	{
		IncreaseValue(val, Upgrades.Health);
	}
	public void IncreaseArmor(Text val)
	{
		IncreaseValue(val, Upgrades.Armor);
	}
	public void IncreaseSpecialAbility(Text val)
	{
		IncreaseValue(val, Upgrades.SpecialAbility);
	}

	void IncreaseValue(Text val, Upgrades upgrade)
	{

		if (spendablePoints == 0)
			return;
		
		switch(upgrade)
		{
			case Upgrades.Damage:

				++damageValue;
				val.text = damageValue.ToString();
				--spendablePoints;

				break;

			case Upgrades.Health:

				++healthValue;
				val.text = healthValue.ToString();
				--spendablePoints;

				break;

			case Upgrades.Armor:

				++armorValue;
				val.text = armorValue.ToString();
				--spendablePoints;

				break;

			case Upgrades.SpecialAbility:

				++specialabilityValue;
				val.text = specialabilityValue.ToString();
				--spendablePoints;

				break;

			default:
				break;
		}
		availableSkillpts.text = spendablePoints.ToString();
	}

	public void NextButtonUpgrade()
	{
		for (int i = 0; i < restartPanel.Length; i++)
		{
			restartPanel[i].gameObject.SetActive(true);
			FadeIn(restartPanel[i]);
		}
	}
	

	public void RestartButton()
	{
		S_GameManager.ResetRoundCounter();
		SceneManager.LoadScene(1);
	}

	public void ExitButton()
	{
		S_GameManager.ResetRoundCounter();
		SceneManager.LoadScene(0);
	}

	#endregion

	void FadeIn(Graphic element)
	{
		element.CrossFadeAlpha(1f, .15f, false);
	}
	void FadeOut(Graphic element)
	{
		element.CrossFadeAlpha(0f, .15f, false);
	}
}
