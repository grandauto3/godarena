﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class S_UIMainMenu : MonoBehaviour
{
    #region MAINMENU
    [Header("Main Menu")]
    [SerializeField]
    GameObject mainMenu;
    [Space]
    [SerializeField]
    Button playButton;
    [SerializeField]
    Button creditsButton;
    [SerializeField]
    Button settingsButton;
    [SerializeField]
    Button exitButton;

    #endregion

    #region LOADINGSCREEN
    [Header("Loading Screen")]
    [SerializeField]
    GameObject loadingScreen;
    [Space]
    [SerializeField]
    Slider loadingSlider;
    [SerializeField]
    Text loadingText;

    #endregion

    #region PLAYMENU
    [Header("Play Menu")]
    [SerializeField]
    GameObject playMenu;
    [Space]
    [SerializeField]
    Button newGameButton;
    [SerializeField]
    Button loadGameButton;
    [SerializeField]
    Button backButton;

    #endregion

    #region OPTIONSMENU

    [Header("Options")]
    [SerializeField]
    GameObject optionsMenu;

    [Space]
    [SerializeField]
    Dropdown resolutionDropdown;
    [SerializeField]
    Dropdown fullscreenDropdown;
    [SerializeField]
    Dropdown qualityDropdown;
    [SerializeField]
    Slider volumeSlider;

    [SerializeField]
    AudioMixer mainMixer;

    //private
    Resolution[] resolutions;

    #endregion

    #region CREDITSMENU

    [SerializeField]
    GameObject creditsMenu;

    #endregion

    void Start()
    {

        #region MAINMENU

        mainMenu.SetActive(true);

        #endregion

        #region LOADINGSCREEN

        loadingScreen.SetActive(false);

        #endregion

        #region PLAYMENU

        playMenu.SetActive(false);

        #endregion

        #region OPTIONSMENU

        optionsMenu.SetActive(false);

        #endregion

        #region CREDITSMENU

        creditsMenu.SetActive(false);

        #endregion
    }

    #region MAINMENU

    public void ClickPlayButton()
    {
        mainMenu.SetActive(false);
        playMenu.SetActive(true);
    }

    public void ClickCreditsButton()
    {
        mainMenu.SetActive(false);
        creditsMenu.SetActive(true);
    }

    public void ClickSettingsButton()
    {
        mainMenu.SetActive(false);
        optionsMenu.SetActive(true);
    }

    public void ClickExitButton()
    {
        Application.Quit();
    }

    #endregion

    #region LOADINGSCREEN

    void LoadLevel()
    {
        StartCoroutine(LoadLevelAsync());
    }

    IEnumerator LoadLevelAsync()
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(1);

        while(!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);

            loadingSlider.value = progress;
            loadingText.text = "Loading... " + progress * 100 + "%";

            yield return null;

        }

    }

    #endregion

    //TODO: LoadGame
    #region PLAYMENU

    public void ClickNewGameButton()
    {
        S_GameManager.ResetRoundCounter();
        playMenu.SetActive(false);
        loadingScreen.SetActive(true);
        LoadLevel();
    }

    public void ClickLoadGameButton()
    {

    }

    public void ClickBackButton()
    {
        playMenu.SetActive(false);
        mainMenu.SetActive(true);
    }

    #endregion

    #region OPTIONSMENU

    public void ChangeResolution(int resolutionIndex)
    {
        Resolution res = resolutions[resolutionIndex];
        Screen.SetResolution(res.width, res.height, Screen.fullScreen);
    }

    public void ChangeFullscreen(int fullscreenIndex)
    {
        switch(fullscreenIndex)
        {
            case 0:
                Screen.fullScreen = true;
                break;

            case 1:

                break;

            case 2:

                Screen.fullScreen = false;
                break;


            default:
                Screen.fullScreen = false;
                break;
        }
    }

    public void ChangeQuality(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
    }

    public void ChangeMasterVolume(float value)
    {
        mainMixer.SetFloat("MainVolume", value);
    }

    public void BackToMenu()
    {
        optionsMenu.SetActive(false);
        mainMenu.SetActive(true);
    }

    void SetQuality()
    {
        qualityDropdown.value = QualitySettings.GetQualityLevel();
        qualityDropdown.RefreshShownValue();
    }

    void SetResolutionsDropdown()
    {
        resolutions = Screen.resolutions;

        resolutionDropdown.ClearOptions();

        List<string> options = new List<string>();

        int currRes = 0;
        for(int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + " x " + resolutions[i].height;
            options.Add(option);

            if(Screen.currentResolution.width == resolutions[i].width && Screen.height == resolutions[i].height)
            {
                currRes = i;
            }
        }

        resolutionDropdown.AddOptions(options);
        resolutionDropdown.value = currRes;
        resolutionDropdown.RefreshShownValue();
    }

    #endregion

    #region CREDITSMENU

    public void BackFromCredits()
    {
        mainMenu.SetActive(true);
        creditsMenu.SetActive(false);
    }

    #endregion
}
