﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[InitializeOnLoad] //nur mit static constructor
[CustomEditor(typeof(S_ExecutePlayer))]
public class S_ExecutePlayerEditor : Editor
{
    /*static */S_ExecutePlayer script;

    /*static */S_RolfController rolfScript;
    /*static */S_LisaController lisaScript;
    /*static */S_ThorstenController thorstenScript;
    /*static */S_JaneController janeScript;
     
    /*static */S_BasePlayer currScript;

    //static S_ExecutePlayerEditor lel;
    
    void OnEnable()
    {
        //lel = new S_ExecutePlayerEditor();
        script = target as S_ExecutePlayer;
        GetAllScripts();
        GetValues();
    }

    void OnDisable()
    {
        SetValues();
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        currScript = GetCurrentScript(script.currCharacter);

        Editor e = CreateEditor(currScript);

        e.DrawDefaultInspector();

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Weapons", EditorStyles.boldLabel);

        currScript.Ammunation = EditorGUILayout.IntField("Total Ammo", currScript.Ammunation);
        DisplayReadOnlyValue("Current Ammo", currScript.CurrentAmmunation);
        currScript.FireRate = EditorGUILayout.FloatField("Fire Rate", currScript.FireRate);
    }

    void SetValues()
    {
        EditorPrefs.SetFloat("RifleDelay", rolfScript.FireRate);
        EditorPrefs.SetFloat("AutoPistolDelay", lisaScript.FireRate);
        EditorPrefs.SetFloat("MGDelay", thorstenScript.FireRate);
        EditorPrefs.SetFloat("singleRifleDelay", janeScript.FireRate);

        EditorPrefs.SetInt("RifleAmmo", rolfScript.Ammunation);
        EditorPrefs.SetInt("AutoPistolAmmo", lisaScript.Ammunation);
        EditorPrefs.SetInt("MGAmmo", thorstenScript.Ammunation);
        EditorPrefs.SetInt("singleRifleAmmo", janeScript.Ammunation);
    }
    void GetValues()
    {
        rolfScript.FireRate = EditorPrefs.GetFloat("RifleDelay");
        lisaScript.FireRate = EditorPrefs.GetFloat("AutoPistolDelay");
        thorstenScript.FireRate = EditorPrefs.GetFloat("MGDelay");
        janeScript.FireRate = EditorPrefs.GetFloat("singleRifleDelay");

        rolfScript.Ammunation = EditorPrefs.GetInt("RifleAmmo");
        lisaScript.Ammunation = EditorPrefs.GetInt("AutoPistolAmmo");
        thorstenScript.Ammunation = EditorPrefs.GetInt("MGAmmo");
        janeScript.Ammunation = EditorPrefs.GetInt("singleRifleAmmo");
    }

    void DisplayReadOnlyValue(string label, int value)
    {
        using (new EditorGUI.DisabledScope(true))
        {
            EditorGUILayout.IntField(label, value);
        }
    }
    void DisplayReadOnlyValue(string label, float value)
    {
        using (new EditorGUI.DisabledScope(true))
        {
            EditorGUILayout.FloatField(label, value);
        }
    }
    void DisplayReadOnlyValue(string label, string value)
    {
        using (new EditorGUI.DisabledScope(true))
        {
            EditorGUILayout.TextField(label,value);
        }
    }

    void GetAllScripts()
    {
        rolfScript = script.GetComponent<S_RolfController>();
        lisaScript = script.GetComponent<S_LisaController>();
        thorstenScript = script.GetComponent<S_ThorstenController>();
        janeScript = script.GetComponent<S_JaneController>();
    }

    S_BasePlayer GetCurrentScript(Character character)
    {
        S_BasePlayer currScript;

        switch (character)
        {
            case Character.Rolf:
                currScript = rolfScript;
                break;
            case Character.Lisa:
                currScript = lisaScript;
                break;
            case Character.Thorsten:
                currScript = thorstenScript;
                break;
            case Character.Jane:
                currScript = janeScript;
                break;
            default:
                currScript = null;
                Debug.LogWarning("Editor: CurrentScript couldn't be set!");
                break;
        }

        return currScript;
    }
}