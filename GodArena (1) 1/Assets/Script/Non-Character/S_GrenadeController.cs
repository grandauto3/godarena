﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S_GrenadeController : MonoBehaviour
{
	[SerializeField]
	float detonateTimer;
	[SerializeField]
	float explosionRadius;
	[SerializeField]
	public float damage;

	Collider[] hittedColliders;

	[HideInInspector]
	public S_RolfController rolfScript;

	// Update is called once per frame
	void Update ()
	{
		Explode();
	}

	void OnDestroy()
	{
		rolfScript.currentCount--;    
	}

	Collider[] Explode()
	{
		detonateTimer -= Time.deltaTime;

		if(detonateTimer <= 0f)
		{
			hittedColliders = Physics.OverlapSphere(transform.position, explosionRadius);
			for(int i = 0; i < hittedColliders.Length; i++)
			{
				if(hittedColliders[i].GetComponent<S_BaseEnemy>())
				{
					hittedColliders[i].GetComponent<S_BaseEnemy>().GetDamage(damage);
				}
			}
		}
		
		Destroy(gameObject, detonateTimer + 0.5f);

		return hittedColliders;
	}
	
}
