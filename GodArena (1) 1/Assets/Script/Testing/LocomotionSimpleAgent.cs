﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(NavMeshAgent))]
public class LocomotionSimpleAgent : MonoBehaviour
{

    NavMeshAgent agent;
    Animator anim;

    Vector2 smoothDeltaPosition;
    Vector2 velocity;

    // Use this for initialization
    void Start()
    {
        smoothDeltaPosition = Vector2.zero;
        velocity = Vector2.zero;

        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();

        agent.updatePosition = false;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 worldDeltaPos = agent.nextPosition - transform.position;


        float dx = Vector3.Dot(transform.right, worldDeltaPos);
        float dy = Vector3.Dot(transform.forward, worldDeltaPos);

        Vector2 deltaPos = new Vector2(dx, dy);

        float smooth = Mathf.Min(1f, Time.deltaTime / .15f);

        smoothDeltaPosition = Vector2.Lerp(smoothDeltaPosition, deltaPos, smooth);

        if(Time.deltaTime > 1e-5f)
            velocity = smoothDeltaPosition / Time.deltaTime;

        bool shouldMove = velocity.magnitude > .5f && agent.remainingDistance > agent.radius;

        anim.SetBool("move", shouldMove);
        anim.SetFloat("Turn", velocity.x);
        anim.SetFloat("Forward", velocity.y);

        //GetComponent<LookAt>().lookAtTargetPosition = agent.steeringTarget + transform.forward;

        LookAt lookAt = GetComponent<LookAt>();
        if(lookAt)
            lookAt.lookAtTargetPosition = agent.steeringTarget + transform.forward;

        // Pull agent towards character
        if(worldDeltaPos.magnitude > agent.radius)
            agent.nextPosition = transform.position + 0.9f * worldDeltaPos;
    }

    void OnAnimatorMove()
    {
        //transform.position = agent.nextPosition;
        Vector3 position = anim.rootPosition;
        position.y = agent.nextPosition.y;
        transform.position = position;

    }
}
