﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
[RequireComponent(typeof(Animator))]
public class Movement : MonoBehaviour
{
    Animator anim;

    bool isWalking;

    void Awake()
    {
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        Turning();
        Walk();
        Move();
    }

    void Move()
    {
        //input vertical
        anim.SetFloat("Forward", 1f);
    }

    //toggle Walk
    void Walk()
    {
        if(Input.GetKeyDown(KeyCode.LeftShift))
        {
            isWalking = !isWalking;
            anim.SetBool("Walk", isWalking);
        }
    }

    void Turning()
    {
        //input horizontal
        anim.SetFloat("Turn", 1f);
    }
}
