﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Animator))]
public class Patrol : MonoBehaviour
{
	[SerializeField]
	bool dying;
	[SerializeField]
	GameObject[] waypoints;

	NavMeshAgent agent;
	Animator anim;
	AnimatorStateInfo stateInfo;

	int destPoint;

	// Use this for initialization
	void Start()
	{
		agent = GetComponent<NavMeshAgent>();
		anim = GetComponent<Animator>();

		stateInfo = anim.GetCurrentAnimatorStateInfo(0);

	}
	
	// Update is called once per frame
	void Update()
	{
		if(waypoints.Length == 0)
		{
			Debug.LogWarning("No Patrol Points set");

			return;
		}

		if(!agent.pathPending && agent.remainingDistance <= 0.5f)
		{
			agent.destination = waypoints[destPoint].transform.position;

			Attack();

			destPoint = Random.Range(0, waypoints.Length);
		}

		if(dying)
			anim.SetTrigger("Died");
	}

	void Attack()
	{
		anim.SetTrigger("attackT");
		StartCoroutine(AttackHalt());
	}

	IEnumerator AttackHalt()
	{
		Debug.Log("LEL");

		yield return new WaitUntil(() => stateInfo.normalizedTime < 1.0f);
	}
}
