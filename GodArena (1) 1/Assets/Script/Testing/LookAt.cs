﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class LookAt : MonoBehaviour
{
	[SerializeField]
	Transform head;
	[SerializeField]
	float lookAtCoolTime;
	[SerializeField]
	float lookAtHeatTime;

	[HideInInspector]
	bool looking;
	[HideInInspector]
	public Vector3 lookAtTargetPosition;


	Vector3 lookAtPosition;
	Animator anim;
	float lookAtWeight;
	// Use this for initialization
	void Start()
	{
		if(!head)
		{
			Debug.LogError("No head attatched!");
			enabled = false;
			return;
		}

		anim = GetComponent<Animator>();
		lookAtTargetPosition = head.position + transform.forward;
		lookAtPosition = lookAtTargetPosition;
	}

	void OnAnimatorIK(int layer)
	{
		lookAtTargetPosition.y = head.position.y;
		float lookAtTargetWeight = looking ? 1f : .0f;

		Vector3 curDir = lookAtPosition - head.position;
		Vector3 futDir = lookAtTargetPosition - head.position;

		curDir = Vector3.RotateTowards(curDir, futDir, 6.28f * Time.deltaTime, float.PositiveInfinity);
		lookAtPosition = head.position + curDir;

		float blendTime = lookAtTargetWeight > lookAtWeight ? lookAtHeatTime : lookAtCoolTime;

		lookAtWeight = Mathf.MoveTowards(lookAtWeight, lookAtTargetWeight, Time.deltaTime / blendTime);
		anim.SetLookAtWeight(lookAtWeight, 0.2f, 0.5f, 0.7f, 0.5f);
		anim.SetLookAtPosition(lookAtPosition);
	}
}
