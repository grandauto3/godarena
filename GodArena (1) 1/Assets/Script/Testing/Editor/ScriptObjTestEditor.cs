﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ScriptObjTest))]
public class ScriptObjTestEditor : Editor
{
    SerializedProperty container;
    SerializedProperty test;

    Editor editor;

    void OnEnable()
    {
        test = serializedObject.FindProperty("selection");
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        serializedObject.Update();

        //EditorGUILayout.PropertyField(test);

        TestObj to = (TestObj)test.enumValueIndex;

        switch(to)
        {
            case TestObj.A:

                container = serializedObject.FindProperty("speed1");

                CreateCachedEditor(container.objectReferenceValue, null, ref editor);

                break;
            case TestObj.B:

                container = serializedObject.FindProperty("speed2");

                CreateCachedEditor(container.objectReferenceValue, null, ref editor);

                break;
            case TestObj.C:

                container = serializedObject.FindProperty("speed3");

                CreateCachedEditor(container.objectReferenceValue, null, ref editor);

                break;
            default:
                break;
        }

        editor.DrawDefaultInspector();
        serializedObject.ApplyModifiedProperties();
    }
}
