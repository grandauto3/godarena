﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum TestObj
{
	A,
	B,
	C,
}

public class ScriptObjTest : MonoBehaviour
{
	//[HideInInspector]
	public TestObj selection;

	[SerializeField]
	InfoContainer speed1;
	[SerializeField]
	InfoContainer speed2;
	[SerializeField]
	InfoContainer speed3;

	[HideInInspector]
	public InfoContainer currInfo;

	[HideInInspector]
	public InfoContainer[] objects;

	NavMeshAgent agent;

	// Use this for initialization
	void Start()
	{
		agent = GetComponent<NavMeshAgent>();
	}
	
	// Update is called once per frame
	void Update()
	{
		ChangeEnum(selection);
		agent.speed = GetCurrentSpeed(selection);
		currInfo.Print();

		Debug.Log("Current Obj: " + selection + " " + agent.speed);
	}

	float GetCurrentSpeed(TestObj obj)
	{
		float f;

		switch(obj)
		{
			case TestObj.A:

				f = speed1.speed;

				break;
			case TestObj.B:

				f = speed2.speed;

				break;
			case TestObj.C:

				f = speed3.speed;

				break;
			default:

				Debug.LogWarning("No speed set");
				f = 0f;

				break;
		}

		return f;
	}

	void ChangeEnum(TestObj lel)
	{
		switch(lel)
		{
			case TestObj.A:

				if(Input.GetKeyDown(KeyCode.E))
					selection = TestObj.B;

				break;
			case TestObj.B:

				if(Input.GetKeyDown(KeyCode.E))
					selection = TestObj.C;

				break;
			case TestObj.C:

				if(Input.GetKeyDown(KeyCode.E))
					selection = TestObj.A;

				break;
			default:
				break;
		}
	}
}
